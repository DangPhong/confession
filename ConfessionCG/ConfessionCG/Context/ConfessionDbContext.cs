﻿using ConfessionCG.Models.Confession;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConfessionCG.Context
{
    public class ConfessionDbContext : DbContext
    {
        public ConfessionDbContext(DbContextOptions<ConfessionDbContext> options) : base(options)
        {

        }
        public DbSet<ConfessionModel> Confessions { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ConfessionModel>().HasData(
                new ConfessionModel() { ID = 1, Story = "djsfhdsfhkd", ImageLink = "dhfjdhfjdf" },
                new ConfessionModel() { ID = 2, Story = "..............", ImageLink = "dh..............fjdhfjdf" }
                );
        }
        public DbSet<ConfessionCG.Models.Confession.ConfessionView> ConfessionView { get; set; }
        public DbSet<ConfessionCG.Models.Confession.ConfessionAdd> ConfessionAdd { get; set; }
    }
}
