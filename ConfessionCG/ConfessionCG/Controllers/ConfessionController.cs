﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ConfessionCG.Models;
using ConfessionCG.Context;
using ConfessionCG.Models.Confession;
using Microsoft.EntityFrameworkCore;

namespace ConfessionCG.Controllers
{
    public class ConfessionController : Controller
    {
        public readonly ConfessionDbContext _dbContext;
        public ConfessionController(ConfessionDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IActionResult> ShowConfession(int? pageNumber)
        {

            //var confession = _dbContext.Confessions.Select(cf => new ConfessionView()
            //{
            //    ID = cf.ID,
            //    Story = cf.Story,
            //    ImageLink= cf.ImageLink
            //}).OrderByDescending(c => c.ID).ToList();
            var confession = (from cf in _dbContext.Confessions
                                  //where cf.isDelete == false
                              select new ConfessionView
                              {
                                  ID = cf.ID,
                                  Story = cf.Story,
                                  ImageLink = cf.ImageLink,
                                  isDelete = cf.isDelete
                              }).OrderByDescending(c => c.ID)/*.ToList()*/;
            int pageSize = 5;
            return View(await PaginatedList<ConfessionView>.CreateAsync(confession.AsNoTracking(), pageNumber ?? 1, pageSize));
            //   return View(confession);
        }

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(ConfessionAdd model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var confession = new ConfessionModel()
                    {
                        Story = model.Story,
                        ImageLink = model.ImageLink
                    };
                    _dbContext.Add(confession);
                    if (_dbContext.SaveChanges() > 0)
                    {
                        TempData["Message"] = "confession has been added successfully.";
                    }
                    else
                    {
                        TempData["Message"] = "Something went wrong, please contact administrator.";
                    }
                }
                catch (Exception ex)
                {

                    return View();
                }
            }
            return View(new ConfessionAdd());
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var confession = _dbContext.Confessions.Find(id);
            confession.isDelete = true;
            _dbContext.SaveChanges();
            return RedirectToAction("ShowConfession");
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
