﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ConfessionCG.Migrations
{
    public partial class seeddata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Confessions",
                columns: new[] { "ID", "ImageLink", "Story", "isDelete" },
                values: new object[] { 1, "dhfjdhfjdf", "djsfhdsfhkd", false });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Confessions",
                keyColumn: "ID",
                keyValue: 1);
        }
    }
}
