﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ConfessionCG.Migrations
{
    public partial class seeddata2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Confessions",
                columns: new[] { "ID", "ImageLink", "Story", "isDelete" },
                values: new object[] { 2, "dh..............fjdhfjdf", "..............", false });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Confessions",
                keyColumn: "ID",
                keyValue: 2);
        }
    }
}
