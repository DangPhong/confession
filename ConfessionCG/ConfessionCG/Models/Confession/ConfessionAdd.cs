﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConfessionCG.Models.Confession
{
    public class ConfessionAdd
    {
        public int ID { get; set; }
        [DisplayName("Câu chuyện của bạn")]
        [MinLength(50, ErrorMessage = "Câu chuyện của bạn phải tối thiếu 20 chữ")]
        [Required(ErrorMessage = "Đừng để trống bạn nhé!")]
        public string Story { get; set; }
        [DisplayName("Ảnh đăng kèm")]
        public string ImageLink { get; set; }
    }
}
