﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ConfessionCG.Models.Confession
{
    public class ConfessionModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string Story { get; set; }
        public string ImageLink { get; set; }
        public bool isDelete { get; set; } = false;
    }
}
