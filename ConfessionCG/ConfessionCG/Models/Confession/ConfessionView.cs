﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace ConfessionCG.Models.Confession
{
    public class ConfessionView
    {
        public int ID { get; set; }
       
        public string Story { get; set; }
        [DisplayName("Image link")]
        public string ImageLink { get; set; }
        public bool isDelete { get; set; }
    }
}
